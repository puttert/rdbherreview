# README #

## Background:
The Society for the Study of Amphibians and Reptiles' (SSAR) **Herptological Review** is an English language peer-reviewed quarterly publication that highlights notes and articles related to the study of reptiles and amphibians. The first issue was published in 1967 
and ever since many observations and notes have been published. Culmulatively, there is a wealth of formal and informal information about reptiles and amphibians worldwide. The complete list of references is available for download on the SSAR website.

**The Reptile Database** is an online "catalogue of all living reptile species and their classification." You can find information about virtually any reptile on this website. Species have their own page and a list of references including articles from Herpetological Review. 

**Task**:
Find out what Herpetological Review references the Reptile Database has not include.  

Method briefly:
1. Download the bibliography from Herpetological Review website
1.  Filter out papers that are already in the Reptile Database (what you sent me)
1.  Filter out titles with amphibian related terms (genera and species names), terms related with obituaries, etc
1.  Detect reptile related terms (species names, some common names)

Data and script to produce the list of Herpetological Review references missing from the Reptile Database can be found in this repository. 

Data sources: 
Reptile Database, AmphibiaWeb, and SSAR.