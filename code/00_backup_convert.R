# 00_backup_convert.R
# backup raw data and convert to open source format

# libraries ---------------------------------------------------------------
library(bib2df)
library(tidyverse)
library(readxl)
library(stringi)

# import raw data ------------------------------------------------------------------
# List of papers already on Reptile Database provided by Peter Uetz
RDB_df_excel <- read_excel("data/raw/HR Reptile Database May 2020.xlsx")

# convert to csv 
write_csv(RDB_df_excel, path = "data/hr_reptiledatabase_2020_05.csv")

# Herpetological Review bib -----------------------------------------------
# Full list of citations for Herpetological Review can be found at https://ssarherps.org/herpetological-review-pdfs/
# https://www.dropbox.com/s/eon0tvnc7kyzfqu/Herpetological_Review_EndNote_Library.enlx?dl=1

# Have to manually import to EndNote and export as a .bib file. 

# bib file contains all HR publications
path_bib <- ("data/raw/Herpetological_Review_EndNote_Library.bib")

HR_df_raw <- bib2df(path_bib)

# Concatenate AUTHORS (plural) as a column with string of author names separated by '; '
HR_df_raw$AUTHORS <- HR_df_raw$AUTHOR %>% stringi::stri_join_list(., sep ="; ") 

# tidying data -----------------------------------------------------------------
# select only the columns we are interested

HR_df_save <- HR_df_raw %>%
  select(BIBTEXKEY, AUTHORS, JOURNAL, NUMBER, PAGES, TITLE, VOLUME, YEAR, URL) %>% 
  # create colume source with format journal# volumne, number (issue), and pages into as used by the Reptile Database
  mutate(SOURCE = paste(JOURNAL, ' ', VOLUME, ' ', '(', NUMBER, ')', ': ', PAGES, sep = '')) %>% 
  select(-JOURNAL, -NUMBER, -PAGES, -VOLUME)

# clean up and save as csv
readr::write_csv(x = HR_df_save, path = 'data/herp_review_bib.csv')

# current reptile genera --------------------------------------------------

# List of current reptile genera provided by Peter Uetz via email on 2020-07-19
# this is used for positive control
reptile_genera_2020_raw <- read_excel("data/raw/reptile genera 2020.xlsx")

# convert to csv 
readr::write_csv(reptile_genera_2020_raw, path = "data/rdb_reptile_genera_2020_07_19.csv")


# current reptile names ---------------------------------------------------
# List of reptile names updated end of year 2019
reptile_names_2019_raw <- read_excel("data/raw/reptile names 2019.xlsx")

readr::write_csv(reptile_names_2019_raw, path = "data/rdb_reptile_names_2019.csv")


# Negative control AMPHIBIANS ---------------------------------------------

# download list of Amphibian names from AmphibianWeb
amphibia_web_url <- "https://amphibiaweb.org/amphib_names.txt"
download.file(amphibia_web_url, destfile = "data/raw/amphibian_list.txt")

amphibian_web_df <- read.delim("data/raw/amphibian_list.txt")
readr::write_csv(amphibian_web_df, path = 'data/amphibia_web_list.csv')
